package publishing

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateArticle(t *testing.T) {
	// Given
	name := "Ikan Asin"
	slug := "ikan-asin"
	body := "Ikan asin terciduk"

	// When
	got := CreateArticle(name, slug, body)

	// Then
	assert.Equal(t, name, got.Name)
	assert.Equal(t, slug, got.Slug)
	assert.Equal(t, body, got.Body)
	assert.NotEmpty(t, got.CreatedAt)
}
