package publishing

import "time"

// Article struct
type Article struct {
	Slug      string
	Name      string
	Body      string
	CreatedAt time.Time
}

// CreateArticle create article
func CreateArticle(name, slug, body string) Article {
	return Article{
		Name:      name,
		Slug:      slug,
		Body:      body,
		CreatedAt: time.Now(),
	}
}
